FROM golang:1.19-bullseye
LABEL author="Thomas Bellembois"

# Copying sources.
WORKDIR /go/src/codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-client-cd/v2/
COPY . .

# Installing.
RUN go install -v ./...
RUN chmod +x /go/bin/codefirst-dockerrunner-client-cd

# Copying entrypoint.
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

USER root
EXPOSE 8081
ENTRYPOINT [ "/entrypoint.sh" ]