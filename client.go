package main

import (
	"context"
	"os"
	"time"

	clientfactory "codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-clientfactory/v2"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-clientfactory/v2/messagesender"
	"nhooyr.io/websocket"
)

type MessageSenderCD struct {
	*messagesender.AbstractMessageSender
	context    context.Context
	cancelFunc context.CancelFunc
}

func NewMessageSenderCD() *MessageSenderCD {
	context, cancelFunc := context.WithTimeout(context.Background(), 60*time.Minute)

	a := &messagesender.AbstractMessageSender{}
	c := &MessageSenderCD{
		a,
		context,
		cancelFunc,
	}

	a.IMessageSender = c

	return c
}

func (m *MessageSenderCD) GetContext() context.Context {
	return m.context
}

func (m *MessageSenderCD) GetCancelFunc() context.CancelFunc {
	return m.cancelFunc
}

func (m *MessageSenderCD) GetDialOptions() *websocket.DialOptions {
	return &websocket.DialOptions{
		HTTPHeader: map[string][]string{"X-Forwarded-User": {os.Getenv("DRONE_REPO_OWNER")}},
	}
}

type ClientCD struct {
	*clientfactory.AbstractClient
}

func NewClientCD() *ClientCD {
	a := &clientfactory.AbstractClient{}
	c := &ClientCD{
		a,
	}

	a.IClient = c

	return c
}
