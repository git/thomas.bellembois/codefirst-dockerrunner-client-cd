#!/usr/bin/env bash
WSScheme=""
WSHost=""
WSPath=""

ImageName=""
ContainerName=""
UserName=""
Password=""
Private=""
Overwrite=""
Env=""
Command=""
Action=""

if [ ! -z "$WSSCHEME" ]
then
    WSScheme="-wsscheme $WSSCHEME"
fi

if [ ! -z "$WSHOST" ]
then
    WSHost="-wshost $WSHOST"
fi

if [ ! -z "$WSPATH" ]
then
    WSPath="-wspath $WSPATH"
fi

if [ ! -z "$IMAGENAME" ]
then
    ImageName="-imagename $IMAGENAME"
fi

if [ ! -z "$CONTAINERNAME" ]
then
    ContainerName="-containername $CONTAINERNAME"
fi

if [ ! -z "$USERNAME" ]
then
    UserName="-username $USERNAME"
fi

if [ ! -z "$PASSWORD" ]
then
    Password="-password $PASSWORD"
fi

if [ ! -z "$PRIVATE" ]
then
    Private="-private"
fi

if [ ! -z "$OVERWRITE" ]
then
    Overwrite="-overwrite"
fi

if [ ! -z "$ACTION" ]
then
    Action="-action $ACTION"
fi

prefix="CODEFIRST_CLIENTCD_ENV_"
ENVS=$(env | awk -F "=" '{print $1}' | grep ".*$prefix.*")

if [ ! -z "$ENVS" ]
then
    Env=""
    arrayEnv=($ENVS)

    for i in "${arrayEnv[@]}"
    do
        envVarName=${i#"$prefix"}
        Env=$Env" -env $envVarName=${!i}"
    done
fi

prefix="CODEFIRST_CLIENTCD_COMMAND_"
COMMANDS=$(env | awk -F "=" '{print $1}' | grep ".*$prefix.*")

if [ ! -z "$COMMANDS" ]
then
    Command=""
    arrayCommand=($COMMANDS)

    for i in "${arrayCommand[@]}"
    do
        Command=$Command" -command '${!i}'"
    done
fi

echo $WSScheme
echo $WSHost
echo $WSPath

echo $Action
echo $ImageName
echo $ContainerName
echo $Private
echo $Overwrite
echo $Env
echo $Command
echo $UserName

#/go/bin
sh -c "/go/bin/codefirst-dockerrunner-client-cd $WSScheme $WSHost $WSPath $ImageName $ContainerName $UserName $Password $Private $Overwrite $Env $Action $Command"
