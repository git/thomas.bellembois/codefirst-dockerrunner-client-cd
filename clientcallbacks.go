package main

import (
	"fmt"
	"os"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"
)

func (c *ClientCD) Log(message string) {
	fmt.Println(message)
}

func (c *ClientCD) ErrorCallback(m messages.WSMessage) {
	fmt.Printf("%+v (%s)\n", m.Error, m.Error.SourceErrorMessage)

	os.Exit(1)
}

func (c *ClientCD) BeforeCreateContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Creating container %s from image %s.", m.Container.Name, m.Container.ImageURL))
	c.Log(fmt.Sprintf("-> container: %+v", m.Container))
}

func (c *ClientCD) AfterCreateContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Container %s created with ID %s.", m.Container.Name, m.Container.ID))
}

func (c *ClientCD) BeforeExecContainerCallback(m messages.WSMessage) {
}

func (c *ClientCD) AfterExecContainerCallback(m messages.WSMessage) {
	fmt.Println(m.Message)

	nbExecCommand--

	if nbExecCommand == 0 {
		os.Exit(0)
	}
}

func (c *ClientCD) BeforeStartContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Starting container %s.", m.Container.Name))
}

func (c *ClientCD) AfterStartContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Container %s started.", m.Container.Name))
	c.Log("Bye !")
	os.Exit(0)
}

func (c *ClientCD) BeforeRemoveContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Removing container %s (ID %s).", m.Container.Name, m.Container.ID))
}

func (c *ClientCD) AfterRemoveContainerCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Container %s removed.", m.Container.Name))

	if action != "create" {
		c.Log("Bye !")
		os.Exit(0)
	}
}

func (c *ClientCD) BeforeGetContainersCallback(m messages.WSMessage) {
	c.Log("Getting the running containers.")
}

func (c *ClientCD) AfterGetContainersCallback(m messages.WSMessage) {
	for _, container := range m.Containers {
		c.Log(fmt.Sprintf("name: %s - id: %s", container.Name, container.ID))
	}
}

func (c *ClientCD) BeforeGetContainerWithSameNameCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Getting containers with name %s.", m.Container.Name))
}

func (c *ClientCD) AfterGetContainerWithSameNameCallback(m messages.WSMessage) {}

func (c *ClientCD) BeforeGetContainerLogCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Getting log for container %s.", m.Container.Name))
}

func (c *ClientCD) AfterGetContainerLogCallback(m messages.WSMessage) {
	c.Log(m.Message)
}

func (c *ClientCD) BeforePullImageCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Pulling image %s.", m.Container.ImageURL))
}

func (c *ClientCD) AfterPullImageCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Image pulled %s.", m.Container.ImageURL))
}

func (c *ClientCD) BeforeGetConfigCallback(m messages.WSMessage) {
	c.Log("Getting server configuration.")
}

func (c *ClientCD) AfterGetConfigCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("maximum allowed containers: %d", m.Config.MaxAllowedContainers))
}

func (c *ClientCD) BeforePingCallback(m messages.WSMessage) {
	c.Log("Pinging server websocket.")
}

func (c *ClientCD) AfterPingCallback(m messages.WSMessage) {
	c.Log("Received pong from server websocket.")
}

func (c *ClientCD) BeforeTestCallback(m messages.WSMessage) {
	c.Log("Sending test message")
}

func (c *ClientCD) AfterTestCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Received test message answer: %s", m.Message))
}

func (c *ClientCD) BeforeTestErrorCallback(m messages.WSMessage) {
	c.Log("Sending error test message")
}

func (c *ClientCD) AfterTestErrorCallback(m messages.WSMessage) {
	c.Log(fmt.Sprintf("Received error test message answer: %s", m.Message))
}
