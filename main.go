package main

import (
	"flag"
	"fmt"
	"os"
)

type StringSliceFlag struct {
	value []string
}

func (s *StringSliceFlag) String() string {
	return fmt.Sprintf("%s", *s)
}

func (s *StringSliceFlag) Set(v string) error {
	s.value = append(s.value, v)
	return nil
}

var (
	imageName, containerName, username, password, action string
	wsScheme, wsHost, wsPath                             string
	private, overwrite                                   bool
	env, command                                         StringSliceFlag
	nbExecCommand                                        int
	splittedCommand                                      []string
)

func init() {
	env = StringSliceFlag{}

	flag.StringVar(&wsScheme, "wsscheme", "ws", "websocket scheme")
	flag.StringVar(&wsHost, "wshost", "dockerrunner:8081", "websocket host")
	flag.StringVar(&wsPath, "wspath", "/", "websocket path")
	flag.StringVar(&action, "action", "create", "action to perform (create, exec)")
	flag.StringVar(&imageName, "imagename", "", "registry image name")
	flag.StringVar(&containerName, "containername", "", "container name")
	flag.StringVar(&username, "username", "", "registry username")
	flag.StringVar(&password, "password", "", "registry password")
	flag.BoolVar(&private, "private", false, "private container")
	flag.BoolVar(&overwrite, "overwrite", false, "overwrite existing container")
	flag.Var(&env, "env", "environment variables (separated by spaces)")
	flag.Var(&command, "command", "command to execute (separated by spaces)")

	flag.Parse()

	fmt.Println("flags:")
	fmt.Printf("-action: %s\n", action)
	fmt.Printf("-imagename: %s\n", imageName)
	fmt.Printf("-containername: %s\n", containerName)
	fmt.Printf("-username: %s\n", username)
	fmt.Printf("-private: %t\n", private)
	fmt.Printf("-overwrite: %t\n", overwrite)
	fmt.Printf("-env: %s\n", env)
	fmt.Printf("-command: %s\n", command)

	if containerName == "" {
		fmt.Println("Missing parameter.")
		os.Exit(1)
	}

	if action == "create" && imageName == "" {
		fmt.Println("Missing parameter for create.")
		os.Exit(1)
	}

	if action == "exec" && len(command.String()) == 0 {
		fmt.Println("Missing parameter for exec.")
		os.Exit(1)
	}
}

// test with:
// docker network create cicd_net
// docker run --name dockerrunner -p80:8081 -v /var/run/docker.sock:/var/run/docker.sock --env TEST=test hub.codefirst.iut.uca.fr/thbellem/codefirst-dockerrunner:latest
// go run . -wshost localhost -imagename mariadb -containername mariadb -env MARIADB_ROOT_PASSWORD=rootpass -env MARIADB_DATABASE=acteur -env MARIADB_USER=user -env MARIADB_PASSWORD=user
// go run . -wshost localhost -imagename nginx -containername nginx
func main() {
	messageSenderCD := NewMessageSenderCD()
	clientCD := NewClientCD()

	clientCD.MessageSender = messageSenderCD.AbstractMessageSender
	clientCD.WSScheme = wsScheme
	clientCD.WSHost = wsHost
	clientCD.WSPath = wsPath

	clientCD.ConnectServer()
	clientCD.Run()

	switch action {
	case "create":
		clientCD.MessageSender.StartContainer(imageName, containerName, username, password, private, overwrite, env.value)
	case "exec":
		nbExecCommand = len(command.value)

		for _, currentCommand := range command.value {
			currentCharIndex := 0
			currentCommandIndex := 0
			isCommandBlock := false
			splittedCommand = []string{""}

			runeCurrentCommand := []rune(currentCommand)

			for currentCharIndex < len(runeCurrentCommand) {
				currentChar := runeCurrentCommand[currentCharIndex]

				switch currentChar {
				case '"', '\'', '`':
					isCommandBlock = !isCommandBlock
					splittedCommand[currentCommandIndex] += string(currentChar)
				case ' ':
					if !isCommandBlock {
						splittedCommand = append(splittedCommand, "")
						currentCommandIndex++
					} else {
						splittedCommand[currentCommandIndex] += string(currentChar)
					}
				default:
					splittedCommand[currentCommandIndex] += string(currentChar)
				}

				currentCharIndex++
			}

			fmt.Printf("executing command: %+v - len: %d\n", splittedCommand, len(splittedCommand))

			clientCD.MessageSender.ExecContainer(containerName, username, splittedCommand, env.value)
		}
	}

	forever := make(chan bool)
	<-forever
}
